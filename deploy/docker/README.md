STEPS:

* sudo docker build -t cacheator .
* sudo docker run -d -p 4000:4000 --name cacheator cacheator

START/STOP

* sudo docker start cacheator
* sudo docker stop cacheator

Add to your /etc/hosts

127.0.0.1       cacheator.com
127.0.0.1       app1.cacheator.com app2.cacheator.com

Finally, in your browser, go to:

http://cacheator.com

Have fun !
