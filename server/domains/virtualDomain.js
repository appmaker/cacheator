'use strict';

var AppHelper = require('./libs/appHelper'),
    url = require('url'),
    colors = require('colors');

module.exports = function virtualDomain(req, res, next) {
  var subdomain = req.host.split('.')[0];
  AppHelper.checkAppExistance(subdomain, function(exists) {
    if (!exists) {
      res.send(404, 'Not found');
    } else {
      redirectToCachedApp(subdomain, req);
      logRedirect(req);
      next();
    }
  });
};

function redirectToCachedApp(subdomain, req) {
  var subdomainPath = getSubdomainPath(subdomain);
  var path = isChrome(req) && isEntryPoint(req) ?
             getSWEntryPoint(req) : getPathname(req);
  var parts = url.parse(req.url);
  req.url = subdomainPath + path + (parts.search || '') + (parts.hash || '');
}

function getSubdomainPath(subdomain) {
  return '/' + subdomain + '/cached';
}

function isChrome(req) {
  return /Chrome\/4\d/.test(req.get('User-Agent'));
}

function isEntryPoint(req) {
  var pathname = url.parse(req.url).pathname;
  return /\/(index.html?)?$/.test(pathname);
}

function getSWEntryPoint(req) {
  return '/index_sw.html';
}

function getPathname(req) {
  return url.parse(req.url).pathname;
}

function logRedirect(req) {
  console.log(
    'Redirecting'.blue,
    req.originalUrl.green,
    '->'.blue,
    req.url.green
  );
}
