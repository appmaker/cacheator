'use strict';

var fs = require('fs-extra'),
    config = require('../config.json'),
    Zip = require('adm-zip'),
    URLHelper = require('./libs/urlHelper'),
    AppHelper = require('./libs/appHelper'),
    piwik = require('./libs/piwik'),
    request = require('request'),
    cacheator = require('../../cacheator-node');

function moveFile(origin, destination, callback) {
  fs.copy(origin, destination, function (error) {
    fs.remove(origin);
    if (error) {
      console.error("moveFile: Error moving file " + origin + " to " +
        destination);
      callback(error);
    }
    else callback(null);
  });
}

function validateAppFields(req, res, onSuccess, onError) {
  // mandatoryFields 'appname' and 'appicon'

  var errorFields = [];
  if (!req.files.appicon.originalFilename) {
    errorFields.push('Missing Field: Please, introduce the app icon');
  }
  if (!req.body.appname) {
    errorFields.push('Missing Field: Please, introduce the App Name');
  }
  if (!req.files.apppackage.originalFilename && !req.params.repo) {
    errorFields.push('Please, Upload your application code first');
  }
  if (errorFields.length > 0) {
      onError(res, errorFields);
  } else {
    onSuccess(req);
  }
}

function processReceivedFiles(files, target, callback) {
  if (files.appicon && files.apppackage) {
    moveFile(files.appicon.path, target + files.appicon.originalFilename,
      function(error) {
        if (error) callback(error);
        else {
          var AppPackagePath = target + files.apppackage.originalFilename;
          moveFile(files.apppackage.path, AppPackagePath, function(error) {
            if (error) callback(error);
            else {
              var zip = new Zip(AppPackagePath);
              zip.extractAllTo(target + 'app');
              callback(null);
            }
          });
        }
      });
  } else {
    callback("Not enough files :(");
  }
}

function createApp(appPath, name, desc, icon, version, appInput, callback) {
  piwik.create(name, function (piwikResponse) {
    // We expect:
    //  - appPath: destination directory were to store the app
    //  - name: name of the app, single word
    //  - desc: description for the app
    //  - icon: path (with filename) to the icon for the app
    //  - version: app version
    //  - appInput: directoy containing the original app
    var appDescriptor = {
      output: appPath,
      input: appInput,
      masterDomain: config.masterDomain,
      publicPort: config.publicPort,
      manifest: {
        name: name,
        description: desc,
        icon: icon,
      },
      piwik: {
        clientCode: piwik.getCode(piwikResponse.value),
        siteId: piwikResponse.value
      }
    };

    try {
      cacheator.init(appDescriptor, function() {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
    } catch (e) {
      if (typeof callback === 'function') {
        callback(e);
      };
    }
  });
};

function errorMessage(res, causeMsg) {
  if (!Array.isArray (causeMsg) ) {
    causeMsg = [causeMsg];
  }
  console.error('Error publishing: ' + causeMsg.join('\n'));
  res.header('content-type', 'text/html');
  res.render(config.masterDomainViews.errorMessage,
    {
      pageName: 'Error!',
      messageError: causeMsg.join('<br/>'),
      baseURL: URLHelper.getBaseURL(),
      piwik: config.piwik
    },
    function (err, stuff) {
      if (!err) {
        res.write(stuff);
        res.end();
      } else {
        console.log(err);
      }
    });
}

module.exports = {
  home: function home(req, res, next) {
    res.header('content-type', 'text/html');
    res.render(config.masterDomainViews.mainForm,
    {
      pageName: "Cache your own app",
      baseURL: URLHelper.getBaseURL(),
      appListURL: URLHelper.getAppListURL(),
      piwik: config.piwik
    },
    function (err, stuff) {
      if (!err) {
        res.write(stuff);
        res.end();
      }
    });
  },

  errorMessage: errorMessage,

  publish: function publish(req, res, next) {
    function send2AppPage() {
      res.header('content-type', 'text/html');
      res.render(config.masterDomainViews.appCreated,
        {
          pageName: "Your new app !",
          app: req.body.appname,
          target: URLHelper.getCachedAppURL(req.body.appname),
          baseURL: URLHelper.getBaseURL(),
          appListURL: URLHelper.getAppListURL(),
          img: URLHelper.getCachedAppIconURL(req.body.appname),
          adminURL: URLHelper.getAppAdminURL(req.body.appname),
          piwik: config.piwik
        },
        function (err, stuff) {
          if (!err) {
            res.write(stuff);
            res.end();
          }
        });
    }
    function processApp() {
      req.body.appname = req.body.appname.toLowerCase();
      AppHelper.createAppFolder(req.body.appname, function(err, path) {
        if (err) {
          errorMessage(res, "Error, when trying to create the folder" + req.body.appname);
          return;
        }
        processReceivedFiles(req.files, path + '/original/', function (error) {
          if (error) {
            errorMessage(res,"Error processing received files");
            return;
          }
          createApp(path + '/cached', req.body.appname, req.body.appdesc,
            path + '/original/' + req.files.appicon.originalFilename,
            req.body.appversion, path + '/original/app', function(err, result) {
              if (err) {
                errorMessage(res, "creating app (caching)");
                return;
              }
              send2AppPage();
            }
          );
        });
      });
    }

    validateAppFields(req, res, processApp, errorMessage);
  },

  apps: function apps(req, res, next) {
    fs.readdir(process.cwd() + config.virtualDomainsBasePath, function(error, folders) {
      if (error) {
        res.send(500, "Error accesing hosting directoy");
        return;
      }

      var apps = [];
      for (var i=0; i<folders.length; i++) {
        var jsonfile = process.cwd() + config.virtualDomainsBasePath + folders[i] + '/cached/app.json';
        try {
          var app = JSON.parse(fs.readFileSync(jsonfile));
          apps.push({
            name: folders[i],
            target: URLHelper.getCachedAppURL(folders[i]),
            img: URLHelper.getCachedAppIconURL(folders[i]),
            adminURL: URLHelper.getAppAdminURL(folders[i]),
            version: app[0].version,
            description: app[0].description
            /* Future implement permissions apps */
            /* Need to change de app.json with the new input checkbox apppermission.
            With this, we can upload this file and appList.ejs with the text commented. */
            /* , permissions: app[0].permissions */
          });
        } catch(e) {
          console.log("Error reading cached app: " + e);
          apps.push({
            name: folders[i] + ' - ERROR Reading cached app',
            target: '#',
            img: '#',
            adminURL: URLHelper.getAppAdminURL(folders[i]),
            version: 'Unknown',
            description: 'Error reading app',
            piwik: config.piwik
          });
        }
      }

      res.header('content-type', 'text/html');
      res.render(config.masterDomainViews.appList,
        {
          pageName: "Available apps",
          apps: apps,
          baseURL: URLHelper.getBaseURL(),
          piwik: config.piwik
        },
        function (err, stuff) {
          if (!err) {
            res.write(stuff);
            res.end();
          }
        });
    });
  },

  appAdmin: function appAdmin(req, res) {
    function deleteApp() {
      AppHelper.deleteAppFolder(req.params.appName, function(err) {
        if (err) {
          res.redirect(URLHelper.getAppAdminURL(req.params.appName));
        } else {
          res.redirect(URLHelper.getAppListURL());
        }
      });
    }

    AppHelper.checkAppExistance(req.params.appName, function (exists) {
      if (!exists) {
        res.status(404).send('The app ' + req.params.appName + ' doesn\'t exists');
        return;
      }

      switch (req.params.action) {
      case 'delete':
        // TODO: Check permissions !
        AppHelper.getPiwikSiteId(req.params.appName, function(err, siteId) {
          if (!err) {
            piwik.delete(siteId, function() {
              deleteApp();
            });
          } else {
            deleteApp();
          }
        });
        break;

      case 'download':
        AppHelper.createAppPackage(req.params.appName, req.query.t, function(error, packageFileName) {
          if (error) {
            console.log(error);
            return res.redirect(URLHelper.getAppAdminURL(req.params.appName));
          }
          console.log(packageFileName);
          res.download(packageFileName);
        });
        break;

      default:
        res.header('content-type', 'text/html');
        res.render(config.masterDomainViews.appAdmin,
          {
            pageName: "Administrate your app",
            appName: req.params.appName,
            baseURL: URLHelper.getBaseURL(),
            adminURL: URLHelper.getAppAdminURL(req.params.appName),
            piwik: config.piwik
          },
          function (err, stuff) {
            if (!err) {
              res.write(stuff);
              res.end();
            }
          });
      }
    });
  }
};

