'use strict';

var config = require('../../config.json'),
    request = require('request');

function createURL(method, params) {
  if (params) {
    params = '&' + params;
  } else {
    params = '';
  }
  return config.piwik.server + '/?module=API&method=' + method +
         '&format=json&token_auth=' + config.piwik.authCode + params;
}

module.exports = {
  create: function(name, cb) {
    if (!cb) cb = function() {};
    var url = createURL('SitesManager.addSite', 'siteName=cached_' + name +
      '&urls[0]=http://' + name + '.' + config.masterDomain);
    request(url, function (error, response, body) {
      cb(JSON.parse(body));
    });
  },

  delete: function(idSite, cb) {
    if (!cb) cb = function() {};
    var url = createURL('SitesManager.deleteSite', 'idSite=' + idSite);
    request(url, function (error, response, body) {
      cb(JSON.parse(body));
    });
  },

  getCode: function(idSite, cb) {

    var clientCode = '\n<!-- Piwik -->';
    clientCode += '\n<script type="text/javascript">';
    clientCode += '\n  var _paq = _paq || [];';
    clientCode += '\n  _paq.push([\'trackPageView\']);';
    clientCode += '\n  _paq.push([\'enableLinkTracking\']);';
    clientCode += '\n  (function() {';
    clientCode += '\n    var u="' + config.piwik.server + '/";';
    clientCode += '\n    _paq.push([\'setTrackerUrl\', u+\'piwik.php\']);';
    clientCode += '\n    _paq.push([\'setSiteId\', ' + idSite + ']);';
    clientCode += '\n    var d=document, g=d.createElement(\'script\'), s=d.getElementsByTagName(\'script\')[0];';
    clientCode += '\n    g.type=\'text/javascript\'; g.async=true; g.defer=true; g.src=u+\'piwik.js\'; s.parentNode.insertBefore(g,s);';
    clientCode += '\n  })();';
    clientCode += '\n</script>';
    clientCode += '\n<noscript><p><img src="' + config.piwik.server + '/piwik.php?idsite=' + idSite + '" style="border:0;" alt="" /></p></noscript>';
    clientCode += '\n<!-- End Piwik Code -->\n';

    return clientCode;
  }
}
