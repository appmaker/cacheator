'use strict';

var fs = require('fs-extra'),
    exec = require('child_process').exec,
    config = require('../../config.json');

function getAppPath(appName) {
  return process.cwd() + config.virtualDomainsBasePath + appName;
}

function checkAppExistance(appName, callback) {
  return fs.exists(getAppPath(appName), callback);
}
module.exports.checkAppExistance = checkAppExistance;

function createAppFolder(appName, callback) {
  checkAppExistance(appName, function (exists) {
    if(exists) {
      console.log(appName + ' already exists');
      callback("Error, app already exists");
      return;
    }

    var appPath = getAppPath(appName);
    console.log(appPath + ' doesn\'t exists. Creating it ;)');
    fs.mkdirSync(appPath);
    fs.mkdirSync(appPath + '/original');
    fs.mkdirSync(appPath + '/cached');
    fs.mkdirSync(appPath + '/packages');
    callback(null, appPath);
  });
}
module.exports.createAppFolder = createAppFolder;

function getPiwikSiteId(appName, callback) {
  console.log('Getting piwik siteId ' + appName + '...');
  checkAppExistance(appName, function (exists) {
    if(!exists) {
      var errMsg = appName + ' doesn\'t exists';
      console.log(errMsg);
      return callback(errMsg);
    }
    var piwikDataFile = getAppPath(appName) + '/piwik.json';
    fs.exists(piwikDataFile, function(exists) {
      if(!exists) {
        var errMsg = piwikDataFile + ' doesn\'t exists';
        console.log(errMsg);
        return callback(errMsg);
      }
      fs.readFile(piwikDataFile, function (e, d) {
        if (e) return callback(e);
        callback(null, JSON.parse(d).idSite);
      });
    });
  });
}
module.exports.getPiwikSiteId = getPiwikSiteId;

function deleteAppFolder(appName, callback) {
  checkAppExistance(appName, function (exists) {
    if(!exists) {
      var errMsg = appName + ' doesn\'t exists';
      console.log(errMsg);
      return callback(errMsg);
    }
    console.log('Removing ' + appName + '...');
    fs.remove(getAppPath(appName), callback);
  });
}
module.exports.deleteAppFolder = deleteAppFolder;

function createAppPackage(appName, target, callback) {
  function checkPackageExistance(packageFileName) {
    if (fs.existsSync(packageFileName)) {
      console.log('Package ' + packageFileName + ' already exists !');
      callback(null, packageFileName);
      return true;
    }
    return false;
  }

  function createZip(packagePath, packageFileName) {
    console.log('Creating ZIP package: ' + packageFileName);
    exec('cd ' + packagePath + '/cached; zip -r ' + packageFileName + ' . ',
      function(err, stdout, stderr) {
        if (err) {
          return callback(err);
        }
/*
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
*/
        callback(null, packageFileName);
      });
  }

  checkAppExistance(appName, function (exists) {
    if(!exists) {
      var errMsg = appName + ' doesn\'t exists';
      console.log(errMsg);
      return callback(errMsg);
    }

    var packagePath = getAppPath(appName);
    var packageFileBaseName = packagePath + '/packages/' + appName + '_' + target;
    var packageFileName = null;
    switch(target) {
    case 'firefox':
      packageFileName = packageFileBaseName + '.zip';
      if (!checkPackageExistance(packageFileName)) {
        createZip(packagePath, packageFileName);
      }
      break;

    case 'chrome':
      packageFileName = packageFileBaseName + '.crx';
      if (!checkPackageExistance(packageFileName)) {
        createZip(packagePath, packageFileName);
      }
      break;

    case 'android':
    case 'windows':
    case 'macos':
      callback(target + ' package not yet implemented');
      break;
    }
  });
}
module.exports.createAppPackage = createAppPackage;
