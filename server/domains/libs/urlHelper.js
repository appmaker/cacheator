'use strict';

var config = require('../../config.json');

function getProtocol() {
  return 'http://';
}

function getHostPort() {
  return config.masterDomain + ':' + config.publicPort;
}

function _getBaseURL() {
  return getProtocol() + getHostPort();
}
module.exports.getBaseURL = _getBaseURL;

module.exports.getAppListURL = function _getAppListURL() {
  return _getBaseURL() + '/apps';
}

module.exports.getErrorURL = function _getErrorURL() {
  return _getBaseURL() + '/error.html';
}

module.exports.getCachedAppURL = function _getCachedAppURL(appName) {
  return getProtocol() + appName + '.' + getHostPort();
}

module.exports.getCachedAppIconURL = function _getCachedAppIconURL(appName) {
  return getProtocol() + appName + '.' + getHostPort() +
         '/icons/icon_128.png';
}

module.exports.getAppAdminURL = function _getAppAdminURL(appName) {
  return _getBaseURL() + '/app/' + appName;
}
