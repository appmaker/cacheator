'use strict';

var fs = require('fs'),
    express = require('express'),
    ecstatic = require('ecstatic'),
    masterDomain = require('./domains/masterDomain'),
    config = require('./config.json'),
    http = require('http'),
    https = require('https');

var credentials = {
  key: fs.readFileSync(config.sslCredentials.key),
  cert: fs.readFileSync(config.sslCredentials.certificate)
};

// If hosted apps directory doesn't exists, we'll create it
var appsPath = process.cwd() + config.virtualDomainsBasePath;
fs.stat(appsPath, function(err, stat) {
  if (err) {
    console.log(appsPath + ' doesn\'t exists. Creating it ;)');
    fs.mkdirSync(appsPath);
  }
});

// Express workflows
var cacheator = express();
cacheator.use(express.bodyParser());
cacheator.set('views', __dirname + '/views');
cacheator.set('view engine', 'ejs');
cacheator.post('/publish', masterDomain.publish);
cacheator.get('/apps', masterDomain.apps);
cacheator.get('/app/:appName', masterDomain.appAdmin);
cacheator.get('/app/:appName/:action', masterDomain.appAdmin);
cacheator.get('/errorMessage', masterDomain.errorMessage);
cacheator.get('/', masterDomain.home);
cacheator.use(express.static(__dirname + '/static'));

var appHosting = express();
appHosting.use(require('./domains/virtualDomain'));
appHosting.use(ecstatic({
  root: __dirname + config.virtualDomainsBasePath,
  cache: 3600,
  baseDir: '/',
  showDir: false,
  autoIndex: true
}));

var badDomain = express();
badDomain.get('/', function(req,res) {
  res.redirect('http://'+config.masterDomain);
});

var app = express();
app.use(express.vhost('*.' + config.masterDomain, appHosting));
app.use(express.vhost(config.masterDomain, cacheator));
app.use(express.vhost('*', badDomain));

http.createServer(app).listen(config.port);
https.createServer(credentials, app).listen(config.sslPort);

console.log('HTTP server at', config.port + '...');
console.log('HTTPS server at', config.sslPort + '...');
