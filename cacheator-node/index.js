'use strict';

var colors = require('colors');
var fs = require('fs');
var cacheator = require('./lib/input');
var cacheatorfs = require('./lib/filesystem');
var appcache = require('./lib/appcache');
var add2home = require('./lib/add2home');
var mutator = require('./lib/dommutator');
var manifest = require('./lib/manifest');
var offliner = require('./lib/offliner');
var Piwik = require('./lib/piwik');
var iconGenerator = require('./lib/icon');

var Cacheator = function Cacheator() {

  var init = function init(config, cb) {
    // Create file structure
    var appSandbox = new cacheatorfs();
    appSandbox.init(config, function onDirs() {
      // Generate icons
      console.log('Generating alternative versions of icons'.green);
      iconGenerator(config.manifest.icon, appSandbox.iconsDir, function() {
        console.log('Generating add2home'.green);
        add2home.populate(config, function() {
          console.log('Generating appcache manifest'.green);
          appcache.generate(config, function() {
            console.log('Adding offliner service worker'.green);
            offliner.generate(config, function() {
              console.log('Mutating original dom'.green);
              mutator.mutateDom(config, function() {
                console.log('Generating manifests'.green);
                manifest.generateManifests(config, function() {
                  console.log('Adding piwik stats'.green);
                  Piwik.populateStats(config, function() {
                    console.log('Done! \\o/'.magenta);
                    if (typeof cb === 'function') {
                      cb();
                    }
                  });
                });
              });
            });
          });
        });
      });
    });
  };

  return {
    'init': init
  };
}();

module.exports = Cacheator;
