'use strict';

var fs = require('fs.extra');
var cheerio = require('cheerio');

var Piwik = function Piwik() {
  function mutateDom(config, cb) {
    var index = config.output + '/index.html';

    fs.exists(index, function(exists) {
      if (!exists) {
        console.log('Could not find index.html'.yellow);
        if (typeof cb === 'function') {
          cb();
        }
        return;
      }

      var $ = cheerio.load(fs.readFileSync(index));

      $('body').append(config.piwik.clientCode);

      // Write content
      fs.writeFile(index,
        $.html(), function(err) {
        if (err) {
          throw err;
        }

        if (typeof cb === 'function') {
          cb();
        }
      });

    });
  };

  function addSiteId(config, cb) {
    var manifestFile = config.output + '/../piwik.json';

    fs.writeFile(manifestFile,
      '{ "idSite": "' + JSON.stringify(config.piwik.siteId) + '" }', function(err) {
      if (err) {
        throw err;
      }
    });

    cb();
  };

  var populate = function(config, cb) {
    addSiteId(config, function() {
      mutateDom(config, cb);
    });
  };

  return {
    'populateStats': populate
  }
}();

module.exports = Piwik;