'use strict';

var fs = require('fs.extra');

var CacheatorFileSystem = function CacheatorFileSystem() {

  var config;

  var createInitialFilesystem = function createInitialFilesystem(cfg, cb) {
    config = cfg;
    // Remove the output dir
    fs.rmrfSync(config.output);

    // Create again the ouptdir
    fs.mkdirpSync(config.output);

    // Copy from input directory to output
    fs.copyRecursive(config.input, config.output, function(err) {
      if (err) {
        throw err;
      }

      try {
        var stats = fs.lstatSync(iconsDir());
        if (!stats || !stats.isDirectory()) {
          fs.mkdirpSync(iconsDir);
        }
      } catch (e) {
        // Not existing
        if (!stats || !stats.isDirectory()) {
          fs.mkdirpSync(iconsDir());
        }
      }

      if (typeof cb === 'function') {
        cb();
      }
    });

  };

  var iconsDir = function iconsDir() {
    if (config) {
      return config.output + '/icons';
    } else {
      return 'icons';
    }
  };

  return {
    'init': createInitialFilesystem,
    get iconsDir() {
      return iconsDir();
    }
  };

};

module.exports = CacheatorFileSystem;
