
"use strict";

var fs = require('fs-extra');

module.exports = {
  generate: function (config, callback) {
    fs.copy(__dirname + '/files/', config.output, function () {
      // TODO: write cache.json
      (typeof callback === 'function') && callback();
    });
  }
};
