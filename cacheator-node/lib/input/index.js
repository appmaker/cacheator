'use strict';

/**
 * Module dependencies
 */
var program = require('commander');
var colors = require('colors');
var mimeOf = require('mime-of');
var path = require('path');
var fs = require('fs');

/**
 * Module exports
 */

module.exports = cacheatorInput;

function checkForFile(file) {
  var mime = mimeOf(file);

  if (mime === undefined || mime.indexOf('image') !== 0) {
    return false;
  }

  return file;
}

function checkForDir(dir) {
  return fs.existsSync(dir) ? dir : false;
}

/**
 * @param {}
 * @return {}
 * @api public
 */
function cacheatorInput(version, args) {
  program.version(version)
    .option('-p, --input <input>', 'Input directory', checkForDir, null)
    .option('-o, --output <output>', 'Output directory')
    .option('-a, --application <application>', 'Web App name')
    .option('-d, --description <description>', 'Description for your web app')
    .option('-i, --icon <icon>', 'Icon of your web app', checkForFile, null)
    .option('-d, --developer [developer]', 'Developer name or email')
    .option('-u, --url [url]', 'Developer\'s home')
    .option('-w, --appVersion [appVersion]', 'App version')
    .option('-m, --masterDomain <masterDomain>', 'Domain that will host the app')
    .parse(args);

  if (!args || args.length == 2) {
    program.help();
    return null;
  }

  var error = false;
  if (program.input === false) {
    console.log('Invalid input directory'.red);
    error = true;
  } else if (!program.input) {
    console.log('Aborting: input directory is mandatory'.red);
    error = true;
  }

  if (!program.output) {
    console.log('Aborting: output directory is mandatory'.red);
    error = true;
  }

  if (!program.application) {
    console.log('Aborting: application option is mandatory'.red);
    error = true;
  }

  if (!program.description) {
    console.log('Aborting: description option is mandatory'.red);
    error = true;
  }

  if (program.icon === false) {
    console.log('Aborting: icon file not recognised as an image'.red);
    error = true;
  } else if (!program.icon){
    console.log('Aborting: icon file is mandatory'.red);
    error = true;
  }

  if (error) {
    return null;
  }

  version = program.appVersion || version;

  var manifest = {
    'name': program.application,
    'description': program.description,
    'version': version,
    'icon': program.icon,
  };

  if (program.developer) {
    if (!manifest.developer) {
      manifest.developer = {};
    }

    manifest.developer.name = program.developer;
  }

  if (program.url) {
    if (!manifest.developer) {
      manifest.developer = {};
    }

    manifest.developer.url = program.url;
  }

  var result = {
    'output': path.resolve(program.output),
    'input': path.resolve(program.input),
    'manifest': manifest
  };

  if (program.masterDomain) {
    result.masterDomain = program.masterDomain;
  }

  return result;
}
