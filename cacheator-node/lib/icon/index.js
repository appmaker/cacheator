'use strict';

var icon = require('icon');
var async = require('async');
var progressBar = require('progress');

function createIcons(image, dir, cb) {
  var files = icon.files;
  files.push({ 'filename': 'icon_60.png', size: 60 });
  files.push({ 'filename': 'icon_128.png', size: 128 });

  var total = files.length;

  var bar = new progressBar('  converting icons [:bar] :percent :etas', {
      complete: '='
    , incomplete: ' '
    , width: 20
    , total: total
  });
  var timer = setInterval(function(){
    if (bar.complete) {
      clearInterval(timer);
      if (typeof cb === 'function') {
        cb();
      }
    }
  }, 100);
  async.waterfall([
      function(next) {
        icon.readImage(image, next)
      },

      function(img, imgBuf, next) {
        async.each(files, function(f, next) {
          icon.writeImage(img, dir + '/' + f.filename, f.size, f.size, next);
          bar.tick();
        }, function(err) {
          next(err, imgBuf)
        });
      },

      icon.bufferToIco
  ], function(err) {
      if (err) console.error(err.message)
  });
}

module.exports = createIcons;
