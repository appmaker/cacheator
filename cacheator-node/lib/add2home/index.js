'use strict';

var fs = require('fs.extra');

var Add2Home = function Add2Home() {

  var params = {
    "name" : "%APP_NAME%", 
    "description" : "%APP_DESC%",
    "version" : "%APP_VERSION%",
    "url" : "%LAUNCHPATH%",
    "baseUrl" : "%BASE_URL%",
    "subdomainUrl" : "",
    "fileUrl" : "%FILE_URL%",
    "chromeItem": "%CHROME_STORE_ID%",
    "icon128" : "%ICON_128%"
  };

  // Given the config, witht he output dir
  // moves resources and generates files necessary
  // for add2home
  var populate = function populate(config, cb) {
    // Copy resources
    var resourcesDir = config.output + '/add2home/';
    fs.copyRecursive(__dirname + '/resources/', resourcesDir, function(err) {
      if (err) {
        throw err;
      }

      // Generate config json for extra add2home parameters
      generateJSON(config, cb);
    });
  };

  var generateJSON = function generateJSON(config, cb) {
    Object.keys(params).forEach(function(key) {
      params[key] = config.manifest[key] || '';
    });

    // Simple params ovewriting
    params.baseUrl = getBaseUrl(config);

    fs.writeFile(config.output + '/app.json',
      JSON.stringify([params]), function(err) {
      if (err) {
        throw err;
      }

      if (typeof cb === 'function') {
        cb();
      }
    });
  };

  function getBaseUrl(config) {
    if (config.masterDomain) {
      return 'http://' + config.manifest.name + '.' +
        config.masterDomain + ':' + config.publicPort;
    } else {
      return 'http://' + config.manifest.name + '.localhost:3000';
    }
  }

  return {
    'populate': populate
  };

}();

module.exports = Add2Home;
