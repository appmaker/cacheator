## Installation

Install with npm:

```
npm install --save cacheator-input
```


## Usage

```
./bin/cacheator -p examples/todo -o output -a 'TODO' -d 'My Todo List' -i ./image.png -w '1.0.0'
```